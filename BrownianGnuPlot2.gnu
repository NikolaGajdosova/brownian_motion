#!/usr/bin/gnuplot
#
# AUTHOR: Nikola Gajdosova

reset

set terminal pngcairo size 350,262 enhanced font 'Verdana,10'
set output 'fname'
set title 'Nahodny Vektor'
set ylabel 'B6'
set xlabel 'B3'
set key at 6.1,1.3
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 2 linewidth 0.2 \
    pointtype 7 pointsize 0.5
set style line 2 \
    linecolor rgb '#dd181f' \
    linetype 2 linewidth 0.2 \
    pointtype 7 pointsize 0.5

plot 'arg1' index 0 with points linestyle 1, \
     'arg1' index 1 with points linestyle 2
