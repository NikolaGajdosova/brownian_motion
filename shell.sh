#!/bin/sh

filename='pointsCC.txt'
filename1='pointsNV.txt'

if [ -e 'experimantalprob.txt' ]; then
rm 'experimantalprob.txt'
else
touch 'experimantalprob.txt'
fi
chmod a+x Brown.cpp
make Brown
touch temp.gnu

for i in {1..3}
do
    ./Brown "xField.txt" $filename 100 $filename1
    n1='pic100''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot1.gnu > temp.gnu
    sed -i 's/arg1/'$filename'/' temp.gnu
    gnuplot temp.gnu
    open $name
    
    n1='picNV100''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot2.gnu > temp.gnu
    sed -i 's/arg1/'$filename1'/' temp.gnu
    gnuplot temp.gnu
    open $name
done

for i in {1..3}
do
    ./Brown "xField.txt" $filename 1000 $filename1
    n1='pic1000''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot1.gnu > temp.gnu
    sed -i 's/arg1/'$filename'/' temp.gnu
    gnuplot temp.gnu
    open $name
    
    n1='picNV1000''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot2.gnu > temp.gnu
    sed -i 's/arg1/'$filename1'/' temp.gnu
    gnuplot temp.gnu
    open $name
done

for i in {1..3}
do
    ./Brown "xField.txt" $filename 10000 $filename1
    n1='pic10000''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot1.gnu > temp.gnu
    sed -i 's/arg1/'$filename'/' temp.gnu
    gnuplot temp.gnu
    open $name
    
    n1='picNV10000''_'
    n2='.png'
    name=$n1$i$n2
    sed 's/fname/'$name'/' BrownianGnuPlot2.gnu > temp.gnu
    sed -i 's/arg1/'$filename1'/' temp.gnu
    gnuplot temp.gnu
    open $name
done
rm temp.gnu
cat 'experimantalprob.txt'
#sed 's/arg1/"'$1'"/' BrownianGnuPlot1.gnu > temp.gnu
#gnuplot temp.gnu
#open introduction1.png
