//
//  main.cpp
//  stochastika_1
//
//  Created by Lea and Shoshana on 02/12/2018.
//  Copyright © 2018 Lea and Shoshana. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <stdlib.h>     /* atoi */

using namespace std;

double* X_field(int n){
    
    ofstream outfile ("xField.txt");
    double h = (1.- 0.)/(n-1),x0 = 0.;
    double *x = new double[n];
    x[0] = x0;
    outfile<<x[0]<<endl;
    
    for(int i = 1; i < n-1; i++){
        
        x[i] = x[0] + i*h;
        outfile<<x[i]<<endl;

    }
    x[n-1] = x[0] + (n-1)*h;
    outfile<<x[n-1]<<endl;
    return x;
}

double sum(int n){
    double np = 0,min = -1, max = 1;
    for(int i = 0; i < n; i++){
        
        double random =
        (max - min) * ( (double)rand() / (double)RAND_MAX ) + min;
        np = np + random;
        
    }
    np = np/sqrt(n);
    
    return np;
    
}

int file_length(string fname){
    
    string line;
    ifstream infile;
    infile.open(fname);
    int i = 0,length=0;
    
    if (!(infile.is_open()))
        exit(EXIT_FAILURE);
    
    while(getline(infile,line)){
        length++;
    }
    
    infile.close();
    
    return length;
    
}

double* READ_X_field(string fname, int length){
    
    string line;
    ifstream infile;
    infile.open(fname);
    int i = 0;
    double *x;

    if (!(infile.is_open()))
        exit(EXIT_FAILURE);
    
    x = new double[length];

    while(!infile.eof()){
        infile>>x[i];
        i++;
        
    }
    infile.close();
    return x;
}
using namespace std;
int main(int argc, const char * argv[]) {
    
    int n = file_length(argv[1]),pocetS=100,pocetT = atoi(argv[3]),temp=0;
    vector<int> v;
    double **trajektoria;
    srand(time(0));
    double *x;
    ofstream outfile (argv[2]);
    ofstream outfileNV (argv[4]);
    ofstream outfilePR;
    outfilePR.open("experimantalprob.txt",ios::out | ios::app);
    trajektoria = new double*[pocetT];

    
    x = READ_X_field(argv[1],n);
    
    
    for(int i = 0; i<pocetT; i++){
        trajektoria[i] = new double[n];
    }
    
    for(int j = 0; j < pocetT; j++){
        
        trajektoria[j][0] = 0.;
        
        for(int i = 0+1; i<n; i++){
            trajektoria[j][i] = trajektoria[j][i-1] + sum(pocetS);
        }
    }
    
    for(int j = 0; j < pocetT; j++){
        for(int i = 0; i<n; i++){
            outfile <<x[i]<<" "<<trajektoria[j][i]<<endl;

        }
        outfile<<""<<endl;

    }
    
    for(int j = 0; j < pocetT; j++){
        
        if((trajektoria[j][3]>0)&&(trajektoria[j][6]<0)&&(trajektoria[j][9]<trajektoria[j][6]))
            v.push_back(j);
        
    }
    
    outfile<<""<<endl;

    for(vector<int>::iterator it = v.begin() ; it != v.end(); ++it){
//        cout<<*it<<endl;
        for(int i = 0; i<n; i++){
            outfile <<x[i]<<" "<<trajektoria[*it][i]<<endl;
        }
        outfile<<""<<endl;

    }
    
//    outfile<<""<<endl;
    
    for(int j = 0; j < pocetT; j++){
        
        outfileNV <<trajektoria[j][3]<<" "<<trajektoria[j][6]<<endl;
 
        outfileNV<<""<<endl;
        
    }
    
    outfileNV<<""<<endl;

    
    for(vector<int>::iterator it = v.begin() ; it != v.end(); ++it){

            outfileNV <<trajektoria[*it][3]<<" "<<trajektoria[*it][6]<<endl;
        
        outfileNV<<""<<endl;
        
    }
    
    outfilePR<<"pocet vsetkych trajektorii: "<<pocetT<<" pocet vyhovujucich trajektorii: "<<v.size()<<" PR: "<<(double)v.size()/pocetT<<endl;


}
