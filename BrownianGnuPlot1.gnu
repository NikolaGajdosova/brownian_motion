#!/usr/bin/gnuplot
#
# AUTHOR: Nikola Gajdosova

reset

set terminal pngcairo size 350,262 enhanced font 'Verdana,10'
set output 'fname'
set title 'Brownian Motion'
set ylabel 'B(t)'
set xlabel 't'
set key at 6.1,1.3
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 2 linewidth 0.2 \
    pointtype 7 pointsize 0.1
set style line 2 \
    linecolor rgb '#dd181f' \
    linetype 2 linewidth 0.2 \
    pointtype 7 pointsize 0.1

plot 'arg1' index 0 with linespoints linestyle 1, \
     'arg1' index 1 with linespoints linestyle 2
